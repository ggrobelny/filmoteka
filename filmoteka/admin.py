from django.contrib import admin
from .models import Film, DodatkoweInfo, Ocena, Aktor


# admin.site.register(Film)
# druga metoda dla admina, dekorator


@admin.register(Film)
class FilmAdmin(admin.ModelAdmin):
    # fields = ["tytul", "opis", "rok"]
    # exclude = ["opis"]
    list_display = ["tytul", "imdb", "rok"]
    list_filter = ["rok", "imdb"]
    search_fields = ["tytul", "opis"]


admin.site.register(DodatkoweInfo)
admin.site.register(Ocena)
admin.site.register(Aktor)
