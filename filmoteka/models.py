from django.db import models

'''
blank false bo tytul nie moze byc pusty
unique true, bo nazwa musi byc unikalna
null true dla rekordow
dalem poprawke po git add . ale paca, yhhh
'''


class DodatkoweInfo(models.Model):
    GATUNEK = {
        (0, "Religijny"),
        (1, "Obyczajowy"),
        (2, "Wojenny"),
        (3, "Komedia"),
        (4, "Horror"),
        (5, "Historyczny"),
        (6, "Sci-Fi"),
        (7, "Dramat")
    }

    czas_trwania = models.PositiveSmallIntegerField(default=0)
    gatunek = models.PositiveSmallIntegerField(default=0, choices=GATUNEK)


class Film(models.Model):
    tytul = models.CharField(max_length=64, blank=False, unique=True)
    rok = models.PositiveSmallIntegerField(default=1960)
    opis = models.TextField(default="")
    premiera = models.DateField(null=True, blank=True)
    imdb = models.DecimalField(max_digits=4, decimal_places=2,
                               null=True, blank=True)
    plakat = models.ImageField(upload_to="plakaty", null=True, blank=True)
    dodatkowe = models.OneToOneField(DodatkoweInfo, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.tytul_z_rokiem()

    def tytul_z_rokiem(self):
        return "{} ({})".format(self.tytul, self.rok)


class Ocena(models.Model):
    recenzja = models.TextField(default="", blank=True)
    gwiazdki = models.PositiveSmallIntegerField(default=5)
    # film to tutaj filtr dla tego modelu, on_delete=models.CASCADE czyli jak filmu nie ma to kasuje oceny
    film = models.ForeignKey(Film, on_delete=models.CASCADE)


class Aktor(models.Model):
    imie = models.CharField(max_length=32)
    nazwisko = models.CharField(max_length=32)
    filmy = models.ManyToManyField(Film)
